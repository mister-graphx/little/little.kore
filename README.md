# Little Kore

Noyau fonctionnel / Framework.

Controleur principal de l'appli, charge le service d'injection de dépendances, initialise le container et les services nécessaires :

- env
- logger
- config
- Events
- Router
- PluginSystem

## Configurations

Les services/providers, sont gérés depuis des fichiers yaml, placés dans un dossier `config/`, et chargés dans le container principal, par le [composant symfony config](https://symfony.com/doc/3.4/components/config.html/). Chaque fichier de configuration peut contenir les informations de `services`, `parameters` et `imports`.

Fichiers de configurations chargés :

| Fichiers | Description |
|--|--|
|`src/config/kore.yaml` `ROOT_PATH/config/kore.yaml` | Kore services |
| `vendor/*/*/config/services.yaml` | Plugins/Extensions |
| `./config/app.yaml` | Main app |



`src/config/services.yaml` imports

```yaml
imports:
    - { resource: 'vendor/*/*/src/config/services.yaml', ignore_errors: true }
    - { resource: 'config/app.yaml', ignore_errors: true }
```

### Ajout de Services

```yaml
services:
    logger:
        class: 'SimpleLog\Logger'
        arguments: ['%log.file%','%log.channel%','%log.level%']
        calls:
            - method: setLogLevel
              arguments: ['%log.level%']

```

## Emmiters

emmitter | env
-- | --
`Kore.loaded` | `$container`
`Kore.onRun` |
`Kore.beforeSetUpRoute` |
`Kore.afterSetUpRoute` |
`Kore.beforeDispatch` |
`Kore.afterDispatch` |
`Kore.error404` |

## Utilisation


Fichier index.php de l'application :

```php
<?php

define('ROOT_PATH', realpath(dirname(__FILE__)) .'/');


require_once(__DIR__ .'vendor/autoload.php');

use Little\Kore\Kore;

$Little = new Kore();
$Little->run();

```


## Librairies :


- Symfony Dependency Injection : https://symfony.com/doc/3.4/components/dependency_injection.html
- Symfony Config : https://symfony.com/doc/3.4/components/config.html
- Symfony HttpFundation : http://symfony.com/fr/doc/current/components/http_foundation/introduction.html
- Symfony Yaml : http://symfony.com/fr/doc/current/components/yaml/introduction.html

- Logger :  
https://github.com/markrogoyski/simplelog-php

- PhRoutes :
A super fast PHP router, with route parameters, restful controllers, filters and reverse routing.
<https://github.com/mrjgreen/phroute>

- Event  
<http://event.thephpleague.com/2.0/emitter/basic-usage/>

- Dot access Data :  
Utilisation d'un syntaxe a point pour accéder au données de type array.
<https://github.com/dflydev/dflydev-dot-access-data>

## Ressources

https://github.com/xssc/awesome-slim
