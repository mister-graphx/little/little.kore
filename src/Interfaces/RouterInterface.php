<?php namespace Little\Kore\Interfaces;
/**
 * Router Interface
 *
*/
interface RouterInterface {
    /**
	 * Router restfull controller helper
	 *
	 * @param string $controller
	 * @param \RouteCollector\Controller $controllerClass
	 * @param callable|function $function route function or callable [class,methode]
	 * @param array $callbacks filters
	 */
    public function controller($controller,$controllerClass,$callbacks);
    /**
	 * Add a route to the router
	 *
	 * @param string $method GET/POST etc.
	 * @param string $route the route URI
	 * @param callable|function $function route function or callable [class,methode]
	 * @param array $callbacks filters
	 */
	public function add($method, $route, $function, $callbacks);
	/**
	 * Processes requests and dispatches the router
	 */
	public function dispatch();
	/**
	 * Add a route filter
	 *
	 * @param string $name
	 * @param function $function
	 */
	public function filter($name, $function);
	/**
	 * Create a route group
	 *
	 * @param array $filters
	 * @param function $function
	 */
	public function group($filters, $function);
	/**
	 * Get the URI of the current request
	 *
	 * @return string
	 */
	public function currentUri();
	/**
	 * Get the base URL of the current request
	 *
	 * @return string
	 */
	public function baseUrl();

}
