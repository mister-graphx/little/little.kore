<?php namespace Little\Kore;
/**
 * Little Kore
 *
 * Core/Framework pakage for the Little CMS
 *
 * @package Little\Kore
 *
*/
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Kernel
 */
class Kore {
    /** @var \SimpleLog\Logger Logger */
    protected $logger;
    /** @var \DotAccessData\Data $config  */
    private $config;

    /** @var \ContainerBuilder $container DI container */
    protected $container;

    /** @var \Little\Events $events Events listeners/emiters */
    protected $events;
    /** @var \Little\Router $router Route collector/Http Request */
    protected $router;
    /**
     * First load Event/Routing ServiceProviders from the default Kore.config,
     * and create an instance
     * Load the application configuration,
     */
    public function __construct() {
        $this->prepare();
        /**
         * Charge la configuration du core
         * ServiceProviders
         */
        $this->container = new ContainerBuilder();
        $loader = new YamlFileLoader($this->container,new FileLocator([__DIR__ , ROOT_PATH ]));
        $loader->load('config/kore.yaml');
        // Env
        if (!$this->container->has('env')) {
            die('Missing environement provider');
        }
        $this->container->get('env');
        // Logger
        if (!$this->container->has('logger')) {
            die('Missing logs provider');
        }
        $this->logger = $this->container->get('logger');
        // Service Provider : Events
        if (!$this->container->has('events')) {
            die('Missing event provider');
        }
        $this->events = $this->container->get('events');
        // Service Provider : Router
        if (!$this->container->has('router')) {
            die('Missing router provider');
        }
        $this->router = $this->container->get('router');
        // Plugins System : Auto load and get services providers
        // from the plugins list in app service parameters
        if ($this->container->has('pluginSystem')) {
            $this->container->get('pluginSystem');
        }
        $this->events->emit('Kore.loaded', $this->container);
        $this->logger->info('Kore is loaded');
        
        // compile the container  render env vars true
        $this->container->compile(true);
        $parameters = $this->container->getParameterBag()->all();

        // Yaml configurations files
        $this->config = $this->container->get('ConfigLoader')->loadConfigFiles();
        //
        $this->config->import($parameters);

        $this->events->emit('Kore.config.loaded', $this->config);
        $this->logger->info('Kore:loaded',['config'=>$this->config]);
    }
  /**
    * Run
    * SetupRoute
    * Router->Dispatch
    */
    public function run() {
        /** event : Kore.onRun */
        $this->events->emit('Kore.onRun', $this->container);
        /** event : Kore.beforeSetUpRoutes */
        $this->events->emit('Kore.beforeSetupRoutes', $this->container);
            $this->setupRoutes();
        $this->events->emit('Kore.afterSetupRoutes', $this->container);
        try {
            $this->events->emit('Kore.beforeDispatch', $this->router);
                $this->router->dispatch();
            $this->events->emit('Kore.afterDispatch', $this->router);
        } catch (\Exception $e) {
            header('Status: HTTP/1.0 404 Not Found');
            $this->events->emit('Kore.error404');
            $this->logger->error('HTTP 404 not Found', ['exceptions'=>$e->getMessage()]);
        }
    }
  /**
    * Set up Routes
    */
    protected function setupRoutes(){

        $this->logger->setOutput(true);

        $this->router->filter('statsStart', function(){
            usleep(mt_rand(100, 10000));
        });

        $this->router->filter('statsComplete', function(){
            $this->logger->info('Page load time: ', ['time' => microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] ]);
        });

        $this->router->add('GET','/kore.config',function(){
            var_dump($this->config->export());

        },['before' => 'statsStart', 'after' => 'statsComplete']);

        $this->router->add('GET','/debug',function(){
            echo "ENV";
            var_dump($_ENV);
            echo "SERVER";
            var_dump($_SERVER);
            echo "CONFIG";
            var_dump($this->config->export());
            echo "EVENTS";
            var_dump($this->events);
            echo "CONTAINER";
            var_dump($this->container);
        });
    }
    /**
     * - define root path
     *
     */
    protected function prepare(){
        if(!defined('ROOT_PATH'))
            define('ROOT_PATH', $_SERVER{'DOCUMENT_ROOT'});

    }

}
