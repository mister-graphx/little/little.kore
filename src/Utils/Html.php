<?php

namespace Little\Kore\Utils;

use Dflydev\DotAccessData\Data;
/**
 * Html class provides html manipulation functions
 *
 */
final class Html
{
    /**
     * The single static instance
     */
    protected static $instance;

    /**
     * Used to return a single instance of the class
     *
     * Checks if instance already exists
     * If it does not exist then it is created
     * The instance is returned
     *
     * @since 1.0.0
     * @return String static::$instance name the instance of the correct child class is returned
     */
    public static function GetInstance($parameters)
    {
        if (static::$instance == null) {
            static::$instance = new static($parameters);
        }
        return static::$instance;
    }

    /**
     * Nav To Html
     *
     * Build a menu ul>li html navigation from a recursive array
     *
     * @param array $array
     * @param bool $top
     * @param array $css predefined styles
    */
    public function navToHTML($array,$config,$top = true, $css = []){

        // Check if somme personal cstyles are set
		(!isset($css) OR $css == null) ? $css = $config->get('navigation_styles') : $css ='' ;

        $stringUtil = UtilitiesFramework::Factory('StringUtils');
		if ($top) {
			$html = '<ul class="'.$css['main'].'">';
		} else {
			$html = '<ul class="'.$css['ul-class'].'">'; // Sub-list
		}

		foreach ($array as $key => $value) {

			if (!is_int($key)) {
				if (preg_match('/^\d+\-/', $key)) {
					list($index, $path) = explode('-', $key, 2);
					$key = $path;
				}

				$title = ucwords(str_replace(['-', '_'], ' ', basename($key)));

				$html .= '<li class="'.$css['has_childs'].' nav-item has-children nav-section-'
						. $stringUtil->slugify($title) . '"><a href="'.$config->get('app.base_url').'/'.$value['path'].'">'
						. $value['title'] . '</a>'
						. self::navToHTML($value['_childs'],$config,$top=false);
				$html .= '</li>';
			} else {
				if(!($value['file'] == 'index.md')){
					$html .= '<li class="nav-item nav-item-' . $stringUtil->slugify($value['title']) . ($value['active'] ? ' active' : '') . '">';
					$html .= '<a href="'.$config->get('app.base_url') . ($value['url'] == '/' ? $value['url'] : '/' . $value['url']) . '">' . $value['title'] . '</a>';
					$html .= '</li>';
				}
			}
		}
		$html .= '</ul>';
		return $html;
	}

}
