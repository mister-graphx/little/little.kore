<?php

namespace Little\Kore\Utils; 

/**
 * FileSystem class provides Files manipulation functions
 *
 *
 */
final class FileSystem
{
    /**
     * The single static instance
     */
    protected static $instance;

    /**
     * Used to return a single instance of the class
     *
     * Checks if instance already exists
     * If it does not exist then it is created
     * The instance is returned
     *
     * @since 1.0.0
     * @return String static::$instance name the instance of the correct child class is returned
     */
    public static function GetInstance($parameters)
    {
        if (static::$instance == null) {
            static::$instance = new static($parameters);
        }
        return static::$instance;
    }

    /**
     * recursive_copy
     *
     *
     *
     * @param $src source path
     * @param $dest destination path
    */
    public function recursive_copy($src,$dest) {
        $dir = opendir($src);
        @mkdir($dest);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    self::recursive_copy($src . '/' . $file,$dest . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dest . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

}
