<?php namespace Little\Kore\Providers ;

/**
 * Plugin system :
 * autoload services containers defined in the plugins list parameters
 *
 * @package \Little\Kore\Providers\PluginSystem
*/
class PluginSystem {
	/** @var array $plugins collection */
    protected $plugins;
    /** @ */
    protected $log;
	/**
	 * On __construct
	 *
	 * @param array $plugins the list of plugins to load, described by their namespace
	 * @param \ContainerBuilder $container
	*/
    public function __construct($plugins, $container){
        $this->log = $container->get('logger');
		$this->load_plugins($plugins,$container);
    }

    /**
     * function load_plugins
     *
     * @param array $plugins - plugin list : array/list of Namespaces
    */
    private function load_plugins($plugins,$container){
        if (!empty($plugins)) {
			foreach ($plugins as $service) {
				if ($container->has($service) == true) {
                    try {
                        $container->get($service);
                        $this->log->info('Service loaded',['Service'=>$service]);
                    } catch (\Exception $e) {
                        $this->log->error('',['exception'=>$e->getMessage().$service]);
                    }
				}
			}
		}else{
            $this->log->info('Empty plugin list');
        }
    }

}
