<?php namespace Little\Kore\Providers;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Parser;
use Dflydev\DotAccessData\Data;
/**
 * Config Loader
 *
*/
class ConfigLoader{

    protected $config;

    public function __construct(array $datas = null){
        $this->config = new Data($datas);
    }
    /**
     * loadConfigFiles
     *
     * @param mixed $configfile config file name
     * @param mixed $exclude Finder excluded
     * @return \DotAccessData\Data $config
     */
    public function loadConfigFiles(string $configfile = 'config.yaml', string $exclude = 'tmp')
    {
        $finder = new Finder();
        $yaml = new Parser();

        $finder->files()
                ->in([__DIR__,
                    ROOT_PATH.'vendor/little/**/src/config/',
                    ROOT_PATH.'config/'])
                ->exclude($exclude)
                ->name($configfile);


        foreach($finder as $file){
            if($file->getContents() && $datas = $yaml->parse($file->getContents())){
                $this->config->import($datas);
            }
        }

        return $this->config;
    }


}
