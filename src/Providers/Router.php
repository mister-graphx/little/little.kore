<?php namespace Little\Kore\Providers ;

use Little\Kore\Interfaces\RouterInterface as RouterInterface;

use Symfony\Component\HttpFoundation\Request;
use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;
/**
 * Router
 *
 * @see https://github.com/mrjgreen/phroute
 * @uses Symfony\Component\HttpFoundation\Request
 * @uses Phroute\
 * @package Little\Kore\Router
*/
class Router implements RouterInterface {

    /**  @var \Phroute\RouteCollector $router */
    protected $router;
    /**  @var \HttpFoundation\Request $request */
	protected $request;

	public function __construct(){
		$this->router = new RouteCollector();
		$this->request = Request::createFromGlobals();
	}

	public function add($method, $route, $function, $callbacks = [] ){
		$this->router->addRoute($method, $route, $function, $callbacks);
	}
    public function controller($controller,$controllerClass,$callbacks){
        $this->router->controller($controller,$controllerClass,$callbacks);
    }
	public function dispatch(){
		$dispatcher = new Dispatcher($this->router->getData());
		echo $dispatcher->dispatch($this->request->getMethod(), $this->request->getPathInfo());
	}

	public function filter($name, $function){
		$this->router->filter($name, $function);
	}

	public function group($filters, $function){
		$this->router->group($filters, $function);
	}
    /** @return string currentUri ex: /path/to/route */
	public function currentUri(){
		return ltrim($this->request->getPathInfo(), '/');
	}
    /** @retur sheme + httphost ex: http://127.0.0.1 */
	public function baseUrl(){
		return $this->request->getSchemeAndHttpHost();
	}


}
