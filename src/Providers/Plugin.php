<?php namespace Little\Kore\PluginSystem ;
/**
 * Plugin
 *
 * base plugin object class,
 * all plugins are extenders of this class.
*/
class Plugin{
    protected $params;// ?

    protected $events;
    protected $router;

    public function __construct($events,$router){
        $this->router= $router;
        $this->events= $events;

        $this->events->addListener('Kore.loaded',[$this, 'onKoreLoaded']);
    }

    public function onKoreLoaded($event,$params){

    }
	/**
     * function setupRoutes
     *
     * add routes
    */
	public function setupRoutes($router){
        $router->add('GET','/plugin',function(){

            echo "Plugin Route";


        });

    }

}
